<?php

/**
 * Invoked after changing the email settings
 *
 * @param $server The server that will be blocked.
 * @param $emails Emails that will remain enabled.
 * @param $message The message values.
 */
function hook_disable_send_mail_alter($server, $emails, &$message) {

}