��          |      �             !     *     =     O  +   j     �     �  ?   �  3   �  !   ,     N  ]  W     �     �     �  +   �  7   &     ^  %   e  K   �  <   �  ?        T                      	                
                     Add more Development Server Disable Send Mail Disable Send Mail Settings Disable sending e-mail to a specific server Email Enter a valid email address Indicate address of the server you want to block sending emails Indicate the emails that you want to remain enabled Manage disable send mail settings Settings Project-Id-Version: PROJECT VERSION
POT-Creation-Date: 2018-10-09 20:26-0300
PO-Revision-Date: 2018-10-09 20:33-0300
Language-Team: LANGUAGE <EMAIL@ADDRESS>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Poedit 2.0.6
Last-Translator: 
Language: pt_BR
 Adicionar mais Servidor de Desenvolvimento Desativa Envio de E-mails Desativa Envio de E-mails - Configurações Desativa o envio de e-mail para um servidor específico E-mail Digite um endereço de e-mail válido Indique o endereço do servidor que você deseja bloquear o envio de emails Indique os e-mails que você deseja que permaneçam ativados Gerenciar as configurações que desabilitam o envio de e-mails Configurações 