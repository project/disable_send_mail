<?php

define('DISABLE_SEND_MAIL_PERMISSION_MANAGE_SETTINGS', 'disable_send_mail_manage_settings');

/**
 * Implements hook_permission().
 */
function disabled_send_mail_permission() {
  return array(
  	DISABLE_SEND_MAIL_PERMISSION_MANAGE_SETTINGS => array(
  		'title' => t('Manage disable send mail settings'),
  	),
  );
}