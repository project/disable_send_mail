<?php

/**
 * Form Settings
 */
function disable_send_mail_settings($form, &$form_state) {	
	$new_email_values = variable_get('disable_send_mail_new_email');
	
	if(isset($form_state['storage']['emails'])){
		if(count($new_email_values) > $form_state['storage']['emails']){
			$form_state['storage']['emails'] = count($new_email_values);
			$form_state['storage']['emails']--;
		}else{
			$form_state['storage']['emails'] = $form_state['storage']['emails'];
		}
	}else{
		if(count($new_email_values) > 1){
			$form_state['storage']['emails'] = count($new_email_values);
			$form_state['storage']['emails']--;
		}else{
			$form_state['storage']['emails'] = 0;
		}
	}

	$form['disable_send_mail'] = array(
		'#type' => 'fieldset',
		'#title' => t('Settings'),
		'#collapsible' => FALSE,
    '#collapsed' => TRUE,
	);
	
 	$form['disable_send_mail']['dev_server'] = array(
 		'#type' => 'textfield',
 		'#title' => t('Development Server'),
 		'#default_value' => variable_get('disable_send_mail_dev_server'),
 		'#description' => t('Indicate address of the server you want to block sending emails') . ' (Ex. xxx.xxxxxx.xxx)',
 		'#maxlength' => 1024,
 		'#required' => TRUE,
 	);
 	
 	$form['disable_send_mail']['new_email'] = array(
 		'#type' => 'textfield',
 		'#title' => t('Email'),
 		'#description' => t('Indicate the emails that you want to remain enabled'),
 		'#default_value' => $new_email_values[0],
 		'#maxlength' => 1024,
 	);
 	
 	$form['disable_send_mail']['emails'] = array(
 		'#type' => 'container',
 		'#tree' => TRUE,
 		'#prefix' => '<div id="emails">',
 		'#suffix' => '</div>',
 	);
 	
 	if(isset($form_state['storage']['emails'])){
 		for($i = 1; $i <= $form_state['storage']['emails']; $i++) {
 			$form['disable_send_mail']['emails'][$i]['new_email'] = array(
 				'#type' => 'textfield',
 				'#maxlength' => 1024,
 				'#default_value' => $new_email_values[$i],
 			);
 		}
 	}
 	
 	$form['disable_send_mail']['add_email'] = array (
 		'#type' => 'button',
 		'#value' => t ('Add more'),
 		'#href' => '',
 		'#ajax' => array (
 			'callback' => 'disable_send_mail_ajax_add_email',
 			'wrapper' => array('emails'),
 		)
 	);
 	
 	$form_state['storage']['emails']++;

 	$form['#validate'][] = 'disable_send_mail_validate';
 	$form['#submit'][] = 'disable_send_mail_submit';

 	return system_settings_form($form);
}

function disable_send_mail_ajax_add_email($form, $form_state) {
	return $form['disable_send_mail']['emails'];
}

/**
 * E-mail Validate
 */
function disable_send_mail_validate($form, $form_state){
	if(isset($form_state['values']['emails'])){
		$emails = array();
		if(isset($form_state['values']['new_email']) && !empty($form_state['values']['new_email'])){
			if(!filter_var($form_state['values']['new_email'], FILTER_VALIDATE_EMAIL)) {
		    form_set_error('disable_send_mail][new_email', t('Enter a valid email address'));
		  }
		}
		foreach($form_state['values']['emails'] as $key => $email){
			if(!empty($email['new_email'])){
				if(!filter_var($email['new_email'], FILTER_VALIDATE_EMAIL)) {
			    form_set_error('disable_send_mail][emails][' . $key . '][new_email', t('Enter a valid email address'));
			  }
			}
		}
	}else{
		if(!empty($form_state['values']['new_email'])){
			if(!filter_var($form_state['values']['new_email'], FILTER_VALIDATE_EMAIL)) {
		    form_set_error('disable_send_mail][new_email', t('Enter a valid email address'));
		  }
		}
	}
}

/**
 * Set Variables
 */
function disable_send_mail_submit($form, $form_state){
	if(!empty($form_state['values']['dev_server'])){
		variable_set('disable_send_mail_dev_server', $form_state['values']['dev_server']);
	}
	
	if(isset($form_state['values']['emails'])){
		$emails = array();
		if(isset($form_state['values']['new_email']) && !empty($form_state['values']['new_email'])){
			$emails[] = $form_state['values']['new_email'];
		}
		foreach($form_state['values']['emails'] as $email){
			if(!empty($email['new_email'])){
				$emails[] = $email['new_email'];
			}
		}
		
		variable_set('disable_send_mail_new_email', $emails);
	}else{
		if(isset($form_state['values']['new_email'])){
			variable_set('disable_send_mail_new_email', array($form_state['values']['new_email']));
		}
	}
}