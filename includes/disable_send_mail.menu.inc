<?php

/**
 * Implements hook_menu().
 */
function disable_send_mail_menu() {
	$paths = array();
	
	$paths['admin/config/system/disable-send-mail'] = array(
		'title' => t('Disable Send Mail Settings'),
		'page callback' => 'drupal_get_form',
		'page arguments' => array('disable_send_mail_settings'),
		'access callback' => 'disable_send_mail_settings_access_callback',
		'file' => 'includes/disable_send_mail.admin.inc',
		'type' => MENU_NORMAL_ITEM,
	);
	
	return $paths;
}

/**
 * Access Callback.
 */
function disable_send_mail_settings_access_callback() {
	global $user;

	$allowed = FALSE;

	if (user_access(DISABLE_SEND_MAIL_PERMISSION_MANAGE_SETTINGS)) {
		$allowed = (bool) get_object_vars(user_load($user->uid));
	}

	return $allowed;
}